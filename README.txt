PayPal Framework module for Drupal
------------------------------------

To install:
	untar the archive with a similar command
		tar xzf paypal_framework-cvs.tar.gz
		cd paypal_framework
	Copy the paypal_framework.module to your drupal modules directory
		cp paypal_framework.module /var/www/htdocs/modules/
	Log into Paypal.com and point your IPN URL to
		http://yoursite/paypal/ipn
		(I should highly suggest setting up SSL)

Objectives:
	To provide the drupal community with a module that would
	reduce the amount of code and effort required to make
	paypal oriented modules.

Features:
	
	IPN Verification Methods
		Currently implimented are libCurl, fsock, and 
		curl binary.  libCurl requires Curl to be
		compiled into php.  fsock needs --enable-sockets
		(or something like that).  Curl Binary needs
		exec() and a curl binary somewhere accessable
		to drupal.  Keep in mind you can't execute
		/usr/bin/curl is php is in safe_mode, but you 
		can execute misc/curl if you upload it there.
		
	Reciever ID filtering
		If the email address isn't in this list, then
		the transaction is discarded.

	Mysql Delayed Inserts
		I didn't know if this was ANSI sql, so I enabled
		it for MySQL only.  If enabled, all INSERTs are
		actually INSERT DELAYED. Which removes alot of load
		on your harddisks.  The INSERT is put into a buffer
		in memory and committed when it has a chance.
	
	Verification Queueing
		The idea came to my head that alot of 'automated
		anti-DoS' systems are probably in place.  Not to
		mention some server admins might see alot of 
		transatctions as malicious activity.  So, to counter
		this, I decided to impliment queuing.  The way 
		it works is if enable, *all* posts that pass the
		reviever ID filter, get stored as raw data in the 
		paypal_queue table.  When cron runs, the _cron
		runs, a number of (or all) of these stored transactions
		are analyzed and processed as if queuing wasn't enabled.

	Real-time Transaction Hook
		Drupal modules that use this framework may react
		to PayPal transactions in real-time.  When a transaction
		is recorded to the database--after verification queueing
		--the framework invokes the _paypal_transaction() hook.
		This reduces the site's reaction time so payments can
		have immediate results instead of waiting hours before
		the next cron update.  This is an event-driven rather
		than a polling architecture.

	View Raw PayPal Transaction Information
		This framework also provides a way to view individual
		transaction's details and can sort by payer ID, item
		number, date, and other methods.
		
		
Examples:
	
	Dynamic Email Filter:
		// This makes it very easy to add users to the filter
		$new_mail=array("mike@death.com","grimm@jordan.edu");
		$email=variable_get("paypal_emails","");
		foreach( $new_mail as $m )
		        $email="\n$m";
		variable_set("paypal_emails",$email);

		// To remove an email

		$h8="grimm@jorban.edu";
		$email=variable_get("paypal_emails","");
		$email=explode("\n",$email);  // Its an array now
		$new_mail=array();
		foreach( $email as $m ){
		        if(strtolower($m)!=strtolower($h8))
		                $new_mail[]=$m;
		}
		$new_mail=implode("\n",$new_mail);
		variable_set("paypal_emails",$new_mail);

	Donations Block

		$funds=db_fetch_object(db_query("SELECT sum(mc_gross) as gross, sum(mc_fee) as fee from {paypal_log} where item_number='GL0001'"));

		$block= t("<i>Total Donations: $</i>".number_format(($funds->gross - $funds->fee),2));
		$form =" ";
		$form.="<input type=\"hidden\" name=\"cmd\" value=\"_xclick\">
		<input type=\"hidden\" name=\"business\" value=\"cracker@gamerslan.us\">
		<input type=\"hidden\" name=\"item_name\" value=\"Gamerslan.us Project Fund\">
		<input type=\"hidden\" name=\"item_number\" value=\"GL0001\">
		<input type=\"hidden\" name=\"cn\" value=\"Friendly Comments\">
		<input type=\"hidden\" name=\"currency_code\" value=\"USD\">
		<input type=\"hidden\" name=\"tax\" value=\"0\">
		";
		$form.="<input type=\"image\" src=\"https://www.paypal.com/en_US/i/btn/x-click-but04.gif\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it's fast, free and secure!\">";  
		
		$block.=form($form,"POST","https://www.paypal.com/cgi-bin/webscr");  
		
		return $block; 

Authors:

Kevin Landreth (crackerjackmack@evilsquid.net)
Nic Ivy        (nji@njivy.org)
Jeff Eaton	   (jeff@viapositiva.net) -- 4.7 update